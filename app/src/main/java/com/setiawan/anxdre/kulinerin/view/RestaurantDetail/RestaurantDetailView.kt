package com.setiawan.anxdre.kulinerin.view.RestaurantDetail

interface RestaurantDetailView {
    fun getData()
    fun showData(Title: String , Cuisines: String , Cost: String , Address: String , City: String , Rating: Float , Votes: String , Delivery: String , Booking: String , ImgPath: String)
    fun showMap(Latitude: String , Longitude: String , Query: String)
    fun showWebsite(Url: String)
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun getLocation()
    fun getUrlRestaurant()
}