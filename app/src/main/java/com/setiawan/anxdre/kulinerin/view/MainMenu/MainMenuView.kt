package com.setiawan.anxdre.kulinerin.view.MainMenu

import RestaurantResponse

interface MainMenuView {
    fun showLoading()
    fun hideLoading()
    fun showSearchPage()
    fun setUserName(Name: String)
    fun getCurrentUser(uid: String)
    fun showUserProfile()
    fun showData(Restaurants: List<RestaurantResponse.Restaurant>)
    fun showError()
    fun showDetail(message: String?)
    fun getLocation()
    fun getGpsPermission()
    fun showErrorLocation()
}