package com.setiawan.anxdre.kulinerin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.setiawan.anxdre.kulinerin.view.signIn.SignIn
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.jetbrains.anko.startActivity

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val mFade:Animation = AnimationUtils.loadAnimation(this,R.anim.transition)
        Title.startAnimation(mFade)

        val timer = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(3000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    startActivity<SignIn>()
                    finish()
                }
            }
        }
        timer.start()
    }
}
