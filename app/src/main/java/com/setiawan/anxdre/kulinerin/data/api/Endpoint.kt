package com.setiawan.anxdre.kulinerin.data.api

object Endpoint {
    private const val HOST = "https://developers.zomato.com/api/v2.1/"
    private const val ENTITY = "&start=1&count=5"
    private const val DETAIL = "restaurant?res_id="
    const val SORT_BY_DISTANCE = "&sort=real_distance&order=asc"
    const val SORT_BY_COST = "&sort=cost&order=asc"
    const val SORT_BY_RATING = "&sort=rating"
    const val TRADITIONAL = "&cuisines=114"

    const val KEY = "01c5936e083cdd8d4d4b7da9f45d6638"
    const val NEARBY_RESTAURANT = "${HOST}search?entity_id=74&entity_type=city&start="
    const val NEARBY_CAFE = "${HOST}search?entity_id=74&entity_type=city&q=cafe&start="
    const val NEARBY_TRADITIONAL_FOOD = "${HOST}search?entity_id=74&entity_type=city&q=makanan%20khas&start="
    const val CUSTOM_REQUEST = "https://developers.zomato.com/api/v2.1/search?entity_id=74&entity_type=city&q="
    const val RESTAURANT_DETAIL = "$HOST$DETAIL"

}