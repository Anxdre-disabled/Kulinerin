package com.setiawan.anxdre.kulinerin.util

import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.setiawan.anxdre.kulinerin.R


fun getImage(Url: String? , Img: ImageView?) {
    AndroidNetworking.get(Url)
            .setTag("imageRequestTag")
            .setPriority(Priority.LOW)
            .setBitmapConfig(Bitmap.Config.ARGB_8888)
            .setBitmapMaxHeight(800)
            .setBitmapMaxWidth(800)
            .build()
            .getAsBitmap(object : BitmapRequestListener {
                override fun onResponse(bitmap: Bitmap) {
                    Img?.setImageBitmap(bitmap)
                }

                override fun onError(error: ANError) {
                    Img?.setImageResource(R.drawable.ic_restaurant_black_24dp)
                }
            })
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}
//fun isNetworkConnected(activity: AppCompatActivity):Boolean{
//    val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//    val networkInfo=connectivityManager.activeNetworkInfo
//    return  networkInfo!=null && networkInfo.isConnected
//}
