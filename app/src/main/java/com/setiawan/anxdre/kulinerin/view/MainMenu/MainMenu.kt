package com.setiawan.anxdre.kulinerin.view.MainMenu

import RestaurantResponse
import android.Manifest
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.adapter.RestaurantAdapter
import com.setiawan.anxdre.kulinerin.data.api.Endpoint
import com.setiawan.anxdre.kulinerin.data.api.RestaurantApi
import com.setiawan.anxdre.kulinerin.util.invisible
import com.setiawan.anxdre.kulinerin.util.visible
import com.setiawan.anxdre.kulinerin.view.ProfileDetail.FragmentProfileDetail
import com.setiawan.anxdre.kulinerin.view.RestaurantDetail.RestaurantDetail
import com.setiawan.anxdre.kulinerin.view.SearchRestaurant.FragmentSearchRestaurant
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity


class MainMenu : AppCompatActivity() , MainMenuView , LocationListener {
    private val mApi by lazy { RestaurantApi() }
    private val mPresenter by lazy { MainMenuPresenter(this , mApi) }
    private var mUserId: String = ""
    private var mCoordinate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        getLocation()

        mUserId = intent.getStringExtra("UserId")
        getCurrentUser(mUserId)

        collapsing_toolbar.setExpandedTitleColor(ContextCompat.getColor(applicationContext , R.color.transparent))
        collapsing_toolbar.setCollapsedTitleTextColor(ContextCompat.getColor(applicationContext , R.color.colorLight))
        toolbar.setOnClickListener { showUserProfile() }
        iv_search_button.setOnClickListener {
            if (et_search.text.isEmpty()) {
            } else {
                showSearchPage()
            }
        }

        ArrayAdapter.createFromResource(this , R.array.category , android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    sp_category.adapter = adapter
                }

        ArrayAdapter.createFromResource(this , R.array.order_by , android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    sp_orderby.adapter = adapter
                }

        sp_category.onItemSelectedListener = (object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
            }

            override fun onItemSelected(parent: AdapterView<*>? , view: View? , position: Int , id: Long) {
                when (position) {
                    0 -> when {
                        sp_orderby.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                    }
                    1 -> when {
                        sp_orderby.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                    }
                    2 -> when {
                        sp_orderby.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                        sp_orderby.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                    }
                }
            }

        })

        sp_orderby.onItemSelectedListener = (object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
            }

            override fun onItemSelected(parent: AdapterView<*>? , view: View? , position: Int , id: Long) {
                when (position) {
                    0 -> when {
                        sp_category.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
                    }
                    1 -> when {
                        sp_category.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_COST , mCoordinate , applicationContext) }
                    }
                    2 -> when {
                        sp_category.selectedItemPosition == 0 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 1 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyCafe(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                        sp_category.selectedItemPosition == 2 -> GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyTraditionalFood(Endpoint.SORT_BY_RATING , mCoordinate , applicationContext) }
                    }
                }
            }

        })

        layout_refresh.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
            layout_refresh.isRefreshing = false
        }

    }


    override fun showLoading() {
        pb_data_load.visible()
    }

    override fun hideLoading() {
        pb_data_load.invisible()
    }

    override fun getCurrentUser(uid: String) {
        mPresenter.getCurrentUserData(uid)
    }

    override fun showUserProfile() {
        val bundle = Bundle()
        bundle.putString("UserId" , mUserId)
        val mFragmentParam = FragmentProfileDetail()
        mFragmentParam.arguments = bundle

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_container_mainmenu , mFragmentParam)
                .addToBackStack(null)
                .setTransition(R.anim.push_right)
                .commit()
    }

    override fun showData(Restaurants: List<RestaurantResponse.Restaurant>) {
        rv_list_restaurant?.let {
            with(rv_list_restaurant) {
                layoutManager = LinearLayoutManager(context)
                adapter = RestaurantAdapter(Restaurants) { it ->
                    startActivity<RestaurantDetail>("restaurantId" to it.restaurant.id)
                }
            }
        }
    }

    override fun showError() {
        hideLoading()
        frame_container_mainmenu.longSnackbar("Make you have gps and network turned on" , "Reload") {
            showLoading()
            GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchDataNearbyRestaurant(Endpoint.SORT_BY_DISTANCE , mCoordinate , applicationContext) }
        }
    }

    override fun showDetail(message: String?) {
        if (message != null) {
            longToast(message)
        }
    }

    override fun showSearchPage() {
        val mQuery = et_search.text.toString()
        val bundle = Bundle()
        bundle.putString("query" , mQuery)
        bundle.putString("location" , mCoordinate)
        val mFragmentParam = FragmentSearchRestaurant()
        mFragmentParam.arguments = bundle

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_container_mainmenu , mFragmentParam)
                .addToBackStack(null)
                .setTransition(R.anim.push_right)
                .commit()
    }

    override fun setUserName(Name: String) {
        tv_user.text = Name
    }

    override fun getLocation() {
        mCoordinate = mPresenter.getCurrentLocation(applicationContext , this)
    }

    override fun getGpsPermission() {
        ActivityCompat.requestPermissions(this , arrayOf(Manifest.permission.ACCESS_FINE_LOCATION , Manifest.permission.ACCESS_COARSE_LOCATION) , 101)
    }

    override fun showErrorLocation() {
        hideLoading()
        frame_container_mainmenu.longSnackbar("Make you have gps and network turned on" , "Reload") {
            getGpsPermission()
        }
    }

    override fun onLocationChanged(location: Location?) {
        mCoordinate = mPresenter.getCurrentLocation(applicationContext , this)
    }

    override fun onStatusChanged(provider: String? , status: Int , extras: Bundle?) {
        mCoordinate = mPresenter.getCurrentLocation(applicationContext , this)
    }

    override fun onProviderEnabled(provider: String?) {
        mCoordinate = mPresenter.getCurrentLocation(applicationContext , this)
    }

    override fun onProviderDisabled(provider: String?) {
        showErrorLocation()
    }
}
