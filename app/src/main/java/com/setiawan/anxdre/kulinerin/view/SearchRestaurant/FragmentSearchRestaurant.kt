package com.setiawan.anxdre.kulinerin.view.SearchRestaurant

import RestaurantResponse
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.adapter.RestaurantAdapter
import com.setiawan.anxdre.kulinerin.data.api.RestaurantApi
import com.setiawan.anxdre.kulinerin.util.invisible
import com.setiawan.anxdre.kulinerin.util.visible
import com.setiawan.anxdre.kulinerin.view.RestaurantDetail.RestaurantDetail
import kotlinx.android.synthetic.main.design_layout_snackbar_include.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.startActivity

class FragmentSearchRestaurant : Fragment() , SearchRestaurantView {
    private val mApi by lazy { RestaurantApi() }
    private val mPresenter by lazy { SearchRestaurantPresenter(this , mApi) }
    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_search , container , false)
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle?) {

        val mCoordinate: String? = arguments?.getString("location")
        val mQuery: String? = arguments?.getString("query")!!.replace(" " , "%20")

        if (mQuery != null && mCoordinate != null) {
            requestData(mQuery , mCoordinate)
        } else {
            showError()
        }

        super.onViewCreated(view , savedInstanceState)
    }

    override fun showLoading() {
        pb_load_search.visible()
    }

    override fun hideLoading() {
        pb_load_search.invisible()
    }

    override fun showError() {
        snackbar_text.snackbar("Something error check your internet connection")
    }

    override fun requestData(mQuery: String , mCoordinate: String) {
        GlobalScope.launch(Dispatchers.Main) { mPresenter.fetchCustomQuery(mQuery , mCoordinate , context!!) }
    }

    override fun showData(Restaurants: List<RestaurantResponse.Restaurant>) {
        rv_search_list?.let {
            with(rv_search_list) {
                layoutManager = LinearLayoutManager(context)
                adapter = RestaurantAdapter(Restaurants) { it ->
                    startActivity<RestaurantDetail>("restaurantId" to it.restaurant.id)
                }
            }
        }
    }
}