package com.setiawan.anxdre.kulinerin.view.ProfileDetail

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.setiawan.anxdre.kulinerin.data.User

class ProfileDetailPresenter(private val mView: ProfileDetailView) {

    fun getUserData(uid: String?) {
        val database = FirebaseDatabase.getInstance()
        val databaseReference = database.getReference("users/$uid")

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                mView.showError()
            }

            override fun onDataChange(data: DataSnapshot) {
                val mUser = data.getValue(User::class.java)
                if (mUser != null) {
                    mView.showData(
                            mUser.name,
                            mUser.email,
                            mUser.phoneNumber
                    )
                }
            }
        })
    }

    fun logoutUser() {
        FirebaseAuth.getInstance().signOut()
        mView.logoutSucces()
    }
}