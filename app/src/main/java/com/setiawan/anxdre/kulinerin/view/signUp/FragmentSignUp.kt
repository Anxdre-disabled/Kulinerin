package com.setiawan.anxdre.kulinerin.view.signUp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.util.invisible
import com.setiawan.anxdre.kulinerin.util.visible
import kotlinx.android.synthetic.main.fragment_sign_up.*
import org.jetbrains.anko.support.v4.toast

class FragmentSignUp : Fragment(), SignUpView {
    private lateinit var mAuth: FirebaseAuth
    private val presenter by lazy { SignUpPresenter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mAuth = FirebaseAuth.getInstance()
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        hideLoading()

        Btn_Register_Fragment.setOnClickListener {

            if (Et_Name.text.toString() == "" || Et_Email_Fragment.text.toString() == "" ||
                    Et_PhoneNumber.text.toString() == "" || Et_Password_Fragment.text.toString() == "" ||
                    Et_ConfirmPassword.text.toString() == "") {
                toast("Please fill all required field")
            } else if (Et_Password_Fragment.text.toString() != Et_ConfirmPassword.text.toString()) {
                toast("Password didnt match")
            } else {
                userRegister(Et_Name.text.toString(),
                        Et_Email_Fragment.text.toString(),
                        Et_PhoneNumber.text.toString(),
                        Et_Password_Fragment.text.toString(),
                        mAuth)
            }
        }
    }

    override fun userRegister(Name: String, Email: String, PhoneNumber: String, Password: String, mAuth: FirebaseAuth) {
        showLoading()
        presenter.userRegister(Name , Email , PhoneNumber , Password , mAuth)
        hideLoading()
    }

    override fun showLoading() {
        Pb_SignUp.visible()
    }

    override fun hideLoading() {
        Pb_SignUp.invisible()
    }

    override fun showError(msg: String) {
        toast("Log in failed because" + msg)
    }

    override fun showSignUpSuccess() {
        toast("Register success")
    }

    override fun removeFragment() {
        fragmentManager!!.popBackStack()
    }

    override fun clearField() {
        Et_Name.setText("")
        Et_Email_Fragment.setText("")
        Et_PhoneNumber.setText("")
        Et_Password_Fragment.setText("")
        Et_ConfirmPassword.setText("")
    }
}