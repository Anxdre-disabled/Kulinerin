package com.setiawan.anxdre.kulinerin.view.MainMenu

import android.content.Context
import android.location.LocationListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.setiawan.anxdre.kulinerin.data.ApiRepository
import com.setiawan.anxdre.kulinerin.data.User
import com.setiawan.anxdre.kulinerin.util.LocationUtil.RequestLocation
import com.setiawan.anxdre.kulinerin.util.LocationUtil.gpsCheck
import com.setiawan.anxdre.kulinerin.util.LocationUtil.gpsNetworkCheck
import com.setiawan.anxdre.kulinerin.util.checkNetworkState
import com.setiawan.anxdre.kulinerin.util.gpsCheckPermission

class MainMenuPresenter(private val mView: MainMenuView ,
                        private val mApi: ApiRepository) {

    suspend fun fetchDataNearbyRestaurant(param: String , location: String , context: Context) {
        mView.getLocation()
        mView.showLoading()
        if (checkNetworkState(context)) {
            val restaurant = mApi.searchNearbyRestaurant(param , location).await().restaurants
            if (restaurant.isNotEmpty()) {
                mView.showData(restaurant)
                mView.hideLoading()


            } else {
                mView.showError()
            }
        } else {
            mView.showError()
            mView.hideLoading()
        }

    }

    suspend fun fetchDataNearbyCafe(param: String , location: String , context: Context) {
        mView.getLocation()
        mView.showLoading()
        if (checkNetworkState(context)) {
            val restaurant = mApi.searchNearbyCafe(param , location).await().restaurants
            if (restaurant.isNotEmpty()) {
                mView.showData(restaurant)
                mView.hideLoading()
            } else {
                mView.showError()
            }
        } else {
            mView.showError()
            mView.hideLoading()
        }
    }

    suspend fun fetchDataNearbyTraditionalFood(param: String , location: String , context: Context) {
        mView.getLocation()
        mView.showLoading()
        if (checkNetworkState(context)) {
            val restaurant = mApi.searchNearbyTraditional(param , location).await().restaurants
            if (restaurant.isNotEmpty()) {
                mView.showData(restaurant)
                mView.hideLoading()


            } else {
                mView.showError()
            }
        } else {
            mView.showError()
            mView.hideLoading()
        }
    }

    fun getCurrentUserData(uid: String) {
        val database = FirebaseDatabase.getInstance()
        val databaseReference = database.getReference("users/$uid")

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                mView.showError()
            }

            override fun onDataChange(data: DataSnapshot) {
                val mUser = data.getValue(User::class.java)
                if (mUser != null) {
                    mView.setUserName(mUser.name)
                }
            }
        })
    }

    fun getCurrentLocation(context: Context , locationListener: LocationListener): String {
        if (!gpsCheckPermission(context)) {
            mView.getGpsPermission()
        }

        if (!gpsCheck(context) && !gpsNetworkCheck(context)) {
            mView.showErrorLocation()
        }
        return RequestLocation.getCurrentLocation(context , locationListener)
    }
}