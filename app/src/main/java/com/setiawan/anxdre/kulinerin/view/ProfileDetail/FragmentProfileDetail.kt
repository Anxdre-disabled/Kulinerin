package com.setiawan.anxdre.kulinerin.view.ProfileDetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.setiawan.anxdre.kulinerin.R
import kotlinx.android.synthetic.main.fragment_profile_detail.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.support.v4.startActivity

class FragmentProfileDetail : Fragment(), ProfileDetailView {
    private val mPresenter by lazy { ProfileDetailPresenter(this) }
    private var mUid: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mUid = arguments?.getString("UserId")
        getData(mUid)

        btn_logout.setOnClickListener({ logoutRequest() })
        super.onViewCreated(view, savedInstanceState)
    }

    override fun getData(UserId: String?) {
        mPresenter.getUserData(UserId)
    }

    override fun showData(Name: String, Email: String, PhoneNumber: String) {
        et_edit_name.setText(Name)
        et_edit_email.setText(Email)
        et_edit_phonenumber.setText(PhoneNumber)
    }

    override fun logoutRequest() {
        mPresenter.logoutUser()
    }

    override fun showError() {
        frame_Container_profile_detail.longSnackbar("Something Error" , "Reload") {
            mPresenter.getUserData(mUid)
        }
    }

    override fun logoutSucces() {
        startActivity<com.setiawan.anxdre.kulinerin.view.signIn.SignIn>()
        activity!!.finish()
    }
}