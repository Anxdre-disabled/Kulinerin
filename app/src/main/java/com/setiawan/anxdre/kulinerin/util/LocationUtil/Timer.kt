package com.setiawan.anxdre.kulinerin.util.LocationUtil

object Timer {
    const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Int = 10
    const val MIN_TIME_BW_UPDATES: Long = 1000 * 60 * 1
}