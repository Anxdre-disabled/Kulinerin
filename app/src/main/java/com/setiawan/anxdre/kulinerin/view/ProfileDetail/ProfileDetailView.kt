package com.setiawan.anxdre.kulinerin.view.ProfileDetail

interface ProfileDetailView {
    fun getData(UserId: String?)
    fun showData(Name: String, Email: String, PhoneNumber: String)
    fun showError()
    fun logoutRequest()
    fun logoutSucces()

}