package com.setiawan.anxdre.kulinerin.view.SearchRestaurant

import android.content.Context
import com.setiawan.anxdre.kulinerin.data.ApiRepository
import com.setiawan.anxdre.kulinerin.util.checkNetworkState

class SearchRestaurantPresenter(private val mView: FragmentSearchRestaurant ,
                                private var mApi: ApiRepository) {

    suspend fun fetchCustomQuery(Param: String? , Coordinate: String? , context: Context) {
        mView.showLoading()
        if (checkNetworkState(context)) {
            val restaurant = mApi.customRequestData(Param!! , Coordinate!!).await().restaurants
            if (restaurant.isNotEmpty()) {
                mView.showData(restaurant)
                mView.hideLoading()
            } else {
                mView.showError()
            }
        } else {
            mView.showError()
            mView.hideLoading()
        }
    }
}