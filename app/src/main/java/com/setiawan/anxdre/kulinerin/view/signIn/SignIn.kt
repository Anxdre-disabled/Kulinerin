package com.setiawan.anxdre.kulinerin.view.signIn

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.util.gpsCheckPermission
import com.setiawan.anxdre.kulinerin.util.invisible
import com.setiawan.anxdre.kulinerin.util.visible
import com.setiawan.anxdre.kulinerin.view.MainMenu.MainMenu
import com.setiawan.anxdre.kulinerin.view.signUp.FragmentSignUp
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class SignIn : AppCompatActivity() , SignInView {
    private val mPresenter by lazy { SignInPresenter(this) }
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        if (!gpsCheckPermission(applicationContext)) {
            ActivityCompat.requestPermissions(this , arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION , android.Manifest.permission.ACCESS_COARSE_LOCATION) , 101)
        }


        hideLoading()
        mAuth = FirebaseAuth.getInstance()
        isUserWasLogin(mAuth)
        Btn_login.setOnClickListener {
            if (TextUtils.isEmpty(Et_Email.text.toString().trim()) || TextUtils.isEmpty(Et_Password.text.toString().trim())) {
                toast("Please fill all the required fields")
            } else {
                userLogIn(Et_Email.text.toString() , Et_Password.text.toString() , mAuth)
            }
        }

        tv_guest.setOnClickListener { showMainMenu() }
    }

    override fun userLogIn(Email: String , Password: String , mAuth: FirebaseAuth) {
        mPresenter.userLogIn(Email , Password , mAuth)
    }

    override fun isUserWasLogin(mAuth: FirebaseAuth) {
        mPresenter.checkUser(mAuth)
    }

    override fun showLoading() {
        Pb_LogIn_Load.visible()
    }

    override fun hideLoading() {
        Pb_LogIn_Load.invisible()
    }

    override fun showMainMenu() {
        startActivity<MainMenu>("UserId" to mAuth.currentUser?.uid.toString())
        finish()
    }

    override fun showSignUp(view: View) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_Container , FragmentSignUp())
                .addToBackStack(null)
                .setTransition(R.anim.push_right)
                .commit()
    }

    override fun showAlert() {
        longToast("Login Failed Please Try Again")
    }
}
