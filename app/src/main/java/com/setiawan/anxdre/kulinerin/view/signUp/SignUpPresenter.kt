package com.setiawan.anxdre.kulinerin.view.signUp

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.setiawan.anxdre.kulinerin.data.User


class SignUpPresenter(private val mView: SignUpView) {
    fun userRegister(Name: String , Email: String , PhoneNumber: String , Password: String , mAuth: FirebaseAuth) {
        mAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val mUser = mAuth.currentUser
                        val user = User(
                                Name,
                                Email,
                                PhoneNumber,
                                mUser!!.uid
                        )
                        val database = FirebaseDatabase.getInstance()
                        val loc = database.getReference("users/${mUser.uid}")
                        loc.setValue(user)
                        mView.clearField()
                        mView.removeFragment()
                        mView.showSignUpSuccess()
                    } else {
                        mView.showError(it.exception.toString())
                    }
                }
    }
}