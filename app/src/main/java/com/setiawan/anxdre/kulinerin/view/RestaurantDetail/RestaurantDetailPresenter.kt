package com.setiawan.anxdre.kulinerin.view.RestaurantDetail

import com.setiawan.anxdre.kulinerin.data.ApiRepository

class RestaurantDetailPresenter(private val mView: RestaurantDetail ,
                                private val mApi: ApiRepository) {

    suspend fun getRestaurantDesc(id: String) {
        mView.showLoading()
        val restaurant = mApi.showDetailRestaurant(id).await()

        mView.showData(restaurant.name , restaurant.cuisines ,
                restaurant.averageCostForTwo.toString() , restaurant.location.address ,
                restaurant.location.city , restaurant.userRating.aggregateRating.toFloat() ,
                restaurant.userRating.votes , restaurant.hasTableBooking.toString().replace("0" , "Yes") ,
                restaurant.hasOnlineDelivery.toString().replace("0" , "Yes") , restaurant.featuredImage)

        mView.hideLoading()
    }

    suspend fun getPlaceLocation(id: String) {
        val restaurant = mApi.showDetailRestaurant(id).await()

        mView.showMap(restaurant.location.latitude , restaurant.location.longitude , restaurant.name)
    }

    suspend fun getPlaceUrl(id: String) {
        val restaurant = mApi.showDetailRestaurant(id).await()

        mView.showWebsite(restaurant.url)
    }
}