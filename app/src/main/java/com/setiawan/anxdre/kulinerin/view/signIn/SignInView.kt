package com.setiawan.anxdre.kulinerin.view.signIn

import android.view.View
import com.google.firebase.auth.FirebaseAuth

interface SignInView {
    fun userLogIn (Email:String,Password:String,mAuth: FirebaseAuth)
    fun isUserWasLogin(mAuth: FirebaseAuth)
    fun showLoading()
    fun hideLoading()
    fun showMainMenu()
    fun showAlert()
    fun showSignUp(view: View)
}