package com.setiawan.anxdre.kulinerin.adapter

import RestaurantResponse
import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.util.getImage
import kotlinx.android.synthetic.main.restaurant_item_adapter.view.*

class RestaurantAdapter(private val mRestaurants: List<RestaurantResponse.Restaurant>,
                        private val mOnClick: (RestaurantResponse.Restaurant) -> Unit)
    : RecyclerView.Adapter<RestaurantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RestaurantViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RestaurantViewHolder(inflater.inflate(R.layout.restaurant_item_adapter, parent, false))
    }

    override fun getItemCount(): Int = mRestaurants.size

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bind(mRestaurants[position], mOnClick)
    }
}

class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @SuppressLint("SetTextI18n")
    fun bind(restaurant: RestaurantResponse.Restaurant, listener: (RestaurantResponse.Restaurant) -> Unit) {
        with(itemView) {
            tv_Food.text = restaurant.restaurant.name
            tv_Street.text = restaurant.restaurant.location.address
            tv_type.text = restaurant.restaurant.cuisines
            tv_main_votes.text = restaurant.restaurant.userRating.votes + " People Votes"
            rate_bar.rating = restaurant.restaurant.userRating.aggregateRating.toFloat()
            getImage(restaurant.restaurant.thumb , iv_Food)

            itemView.setOnClickListener { listener(restaurant) }
        }
    }
}