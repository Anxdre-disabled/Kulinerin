package com.setiawan.anxdre.kulinerin.view.RestaurantDetail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.setiawan.anxdre.kulinerin.R
import com.setiawan.anxdre.kulinerin.data.api.RestaurantApi
import com.setiawan.anxdre.kulinerin.util.getImage
import com.setiawan.anxdre.kulinerin.util.invisible
import com.setiawan.anxdre.kulinerin.util.visible
import kotlinx.android.synthetic.main.activity_restaurant_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.toast


class RestaurantDetail : AppCompatActivity() , RestaurantDetailView {
    private val mRestaurantId by lazy { intent.getStringExtra("restaurantId") }
    private val mApi by lazy { RestaurantApi() }
    private val mPresenter by lazy { RestaurantDetailPresenter(this , mApi) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_detail)

        getData()
        btn_open_map.setOnClickListener { getLocation() }
        btn_more_detail.setOnClickListener { getUrlRestaurant() }
    }

    override fun getData() {
        GlobalScope.launch(Dispatchers.Main) { mPresenter.getRestaurantDesc(mRestaurantId) }
    }

    @SuppressLint("SetTextI18n")
    override fun showData(Title: String , Cuisines: String , Cost: String , Address: String ,
                          City: String , Rating: Float , Votes: String , Delivery: String , Booking: String ,
                          ImgPath: String) {

        tv_title_restaurant.text = Title
        tv_cuisines.text = Cuisines
        tv_average_cost.text = "Rp.$Cost / 2 orang"
        tv_address.text = Address
        tv_city.text = City
        rb_activity_detail.rating = Rating
        tv_rating.text = "$Rating / $Votes"
        tv_has_delivery.text = "Delivery online : $Delivery"
        tv_has_booking.text = "Booking meja : $Delivery"
        getImage(ImgPath , iv_cover_restaurant)
    }

    override fun showMap(Latitude: String , Longitude: String , Query: String) {
        val intent = Intent(android.content.Intent.ACTION_VIEW ,
                Uri.parse("geo:$Latitude,$Longitude?q=${Query.replace(" " , "+")}"))
        startActivity(intent)
    }

    override fun showWebsite(Url: String) {
        val intent = Intent(android.content.Intent.ACTION_VIEW ,
                Uri.parse(Url))
        startActivity(intent)
    }

    override fun showLoading() {
        pb_data_detail_load.visible()
    }

    override fun showError() {
        toast("Something error check your internet connection")
    }

    override fun hideLoading() {
        pb_data_detail_load.invisible()
    }

    override fun getLocation() {
        GlobalScope.launch(Dispatchers.Main) { mPresenter.getPlaceLocation(mRestaurantId) }
    }

    override fun getUrlRestaurant() {
        GlobalScope.launch(Dispatchers.Main) { mPresenter.getPlaceUrl(mRestaurantId) }
    }
}
