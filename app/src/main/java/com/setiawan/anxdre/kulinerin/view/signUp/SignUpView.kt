package com.setiawan.anxdre.kulinerin.view.signUp

import com.google.firebase.auth.FirebaseAuth

interface SignUpView {
    fun userRegister(Name: String, Email: String, PhoneNumber: String, Password: String, mAuth: FirebaseAuth)
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showSignUpSuccess()
    fun removeFragment()
    fun clearField()
}