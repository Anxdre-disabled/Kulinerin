package com.setiawan.anxdre.kulinerin.view.signIn

import com.google.firebase.auth.FirebaseAuth

class SignInPresenter (private val mView:SignInView){
    fun userLogIn(Email: String, Password: String,mAuth:FirebaseAuth) {
        mView.showLoading()
        mAuth.signInWithEmailAndPassword(Email,Password).addOnCompleteListener {
            if (!it.isSuccessful) {
                mView.hideLoading()
                mView.showAlert()
            } else if (it.isSuccessful) {
                mView.showMainMenu()
            }
        }
    }

    fun checkUser(mAuth:FirebaseAuth){
        if (mAuth.currentUser != null){
            mView.showMainMenu()
        }
    }
}
