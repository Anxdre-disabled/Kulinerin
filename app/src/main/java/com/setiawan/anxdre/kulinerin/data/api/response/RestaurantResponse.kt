
import com.google.gson.annotations.SerializedName

data class RestaurantResponse(
        @SerializedName("results_found")
        val resultsFound: Int ,
        @SerializedName("results_start")
        val resultsStart: Int ,
        @SerializedName("results_shown")
        val resultsShown: Int ,
        @SerializedName("restaurants")
        val restaurants: List<Restaurant>
) {

    data class Restaurant(
            @SerializedName("restaurant")
            val restaurant: Restaurant
    ) {
        data class Restaurant(
                @SerializedName("R")
                val r: R ,
                @SerializedName("apikey")
                val apikey: String ,
                @SerializedName("id")
                val id: String ,
                @SerializedName("name")
                val name: String ,
                @SerializedName("url")
                val url: String ,
                @SerializedName("location")
                val location: Location ,
                @SerializedName("switch_to_order_menu")
                val switchToOrderMenu: Int ,
                @SerializedName("cuisines")
                val cuisines: String ,
                @SerializedName("average_cost_for_two")
                val averageCostForTwo: Int ,
                @SerializedName("price_range")
                val priceRange: Int ,
                @SerializedName("currency")
                val currency: String ,
                @SerializedName("offers")
                val offers: List<Any> ,
                @SerializedName("opentable_support")
                val opentableSupport: Int ,
                @SerializedName("is_zomato_book_res")
                val isZomatoBookRes: Int ,
                @SerializedName("mezzo_provider")
                val mezzoProvider: String ,
                @SerializedName("is_book_form_web_view")
                val isBookFormWebView: Int ,
                @SerializedName("book_form_web_view_url")
                val bookFormWebViewUrl: String ,
                @SerializedName("book_again_url")
                val bookAgainUrl: String ,
                @SerializedName("thumb")
                val thumb: String ,
                @SerializedName("user_rating")
                val userRating: UserRating ,
                @SerializedName("photos_url")
                val photosUrl: String ,
                @SerializedName("menu_url")
                val menuUrl: String ,
                @SerializedName("featured_image")
                val featuredImage: String ,
                @SerializedName("has_online_delivery")
                val hasOnlineDelivery: Int ,
                @SerializedName("is_delivering_now")
                val isDeliveringNow: Int ,
                @SerializedName("include_bogo_offers")
                val includeBogoOffers: Boolean ,
                @SerializedName("deeplink")
                val deeplink: String ,
                @SerializedName("is_table_reservation_supported")
                val isTableReservationSupported: Int ,
                @SerializedName("has_table_booking")
                val hasTableBooking: Int ,
                @SerializedName("events_url")
                val eventsUrl: String ,
                @SerializedName("establishment_types")
                val establishmentTypes: List<Any>
        ) {
            data class UserRating(
                    @SerializedName("aggregate_rating")
                    val aggregateRating: String ,
                    @SerializedName("rating_text")
                    val ratingText: String ,
                    @SerializedName("rating_color")
                    val ratingColor: String ,
                    @SerializedName("votes")
                    val votes: String
            )

            data class R(
                    @SerializedName("res_id")
                    val resId: Int
            )

            data class Location(
                    @SerializedName("address")
                    val address: String ,
                    @SerializedName("locality")
                    val locality: String ,
                    @SerializedName("city")
                    val city: String ,
                    @SerializedName("city_id")
                    val cityId: Int ,
                    @SerializedName("latitude")
                    val latitude: String ,
                    @SerializedName("longitude")
                    val longitude: String ,
                    @SerializedName("zipcode")
                    val zipcode: String ,
                    @SerializedName("country_id")
                    val countryId: Int ,
                    @SerializedName("locality_verbose")
                    val localityVerbose: String
            )
        }
    }
}