package com.setiawan.anxdre.kulinerin.view.SearchRestaurant

interface SearchRestaurantView {
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun requestData(mQuery: String , mCoordinate: String)
    fun showData(Restaurants: List<RestaurantResponse.Restaurant>)
}