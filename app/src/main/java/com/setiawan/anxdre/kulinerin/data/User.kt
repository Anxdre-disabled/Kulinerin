package com.setiawan.anxdre.kulinerin.data

data class User(val name: String = "", val email: String = ""
                , val phoneNumber: String = "", val uid: String = "")