package com.setiawan.anxdre.kulinerin.data.api

import RestaurantResponse
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.setiawan.anxdre.kulinerin.data.ApiRepository
import com.setiawan.anxdre.kulinerin.data.api.response.RestaurantDetailResponse
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

@Suppress("UNCHECKED_CAST")
class RestaurantApi : ApiRepository {

    override fun searchNearbyRestaurant(param: String , location: String): Deferred<RestaurantResponse> = reqData(
            Endpoint.NEARBY_RESTAURANT + location + param , RestaurantResponse::class.java
    ) as Deferred<RestaurantResponse>

    override fun searchNearbyCafe(param: String , location: String): Deferred<RestaurantResponse> = reqData(
            Endpoint.NEARBY_CAFE + location + param , RestaurantResponse::class.java

    ) as Deferred<RestaurantResponse>

    override fun searchNearbyTraditional(param: String , location: String): Deferred<RestaurantResponse> = reqData(
            Endpoint.NEARBY_TRADITIONAL_FOOD + location + Endpoint.TRADITIONAL + param , RestaurantResponse::class.java
    ) as Deferred<RestaurantResponse>

    override fun showDetailRestaurant(restaurantId: String): Deferred<RestaurantDetailResponse> = reqData(
            Endpoint.RESTAURANT_DETAIL + restaurantId , RestaurantDetailResponse::class.java
    ) as Deferred<RestaurantDetailResponse>

    override fun customRequestData(param: String , location: String): Deferred<RestaurantResponse> = reqData(
            "${Endpoint.CUSTOM_REQUEST}$param$location${Endpoint.SORT_BY_DISTANCE}" , RestaurantResponse::class.java
    ) as Deferred<RestaurantResponse>

    private fun reqData(url: String , type: Class<*>): Deferred<Any> {
        return GlobalScope.async {
            val response = AndroidNetworking
                    .get(url)
                    .setTag("fetchData")
                    .setPriority(Priority.MEDIUM)
                    .addHeaders("user-key" , Endpoint.KEY)
                    .build()
                    .executeForObject(type)
            if (!response.isSuccess) {
                throw response.error
            }
            response.result

        }
    }
}