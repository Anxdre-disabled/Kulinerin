package com.setiawan.anxdre.kulinerin.data

import RestaurantResponse
import com.setiawan.anxdre.kulinerin.data.api.response.RestaurantDetailResponse
import kotlinx.coroutines.Deferred

interface ApiRepository {
    fun searchNearbyRestaurant(param: String , location: String): Deferred<RestaurantResponse>
    fun searchNearbyCafe(param: String , location: String): Deferred<RestaurantResponse>
    fun searchNearbyTraditional(param: String , location: String): Deferred<RestaurantResponse>
    fun customRequestData(param: String , location: String): Deferred<RestaurantResponse>
    fun showDetailRestaurant(restaurantId: String): Deferred<RestaurantDetailResponse>
}